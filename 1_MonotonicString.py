def checkMonotony(s):
    value= s[0]
    mnd, mni = 0, 0
    for ch in s:
        if ch > value:
            mnd, value = 1, ch
        elif ch < value:
            mni, value = 1, ch
        if mnd == 1 and mni == 1:
            return "NONE"
    if mnd:
        return "MND"
    else: 
        return "MNI"

s = "axcd"
s1 = "abcd"
s2 = "dcba"
print(checkMonotony(s))
print(checkMonotony(s1))
print(checkMonotony(s2))
        

