MON = {'JAN':1, 'FEB':2, 'MAR':3, 'APR':4, 'MAY':5, 'JUN':6, 'JULY':7, 'AUG':8, 'SEP':9, 'OCT':10, 'NOV':11, 'DEC':12}
daysinMonth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
cumDaysMon = [0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 274, 304, 335]

def parseDate(s):
    m = ""
    d = int((s[0:2]).strip(", "))
    y = int(s[len(s)-4:len(s)])
    for i in range(2,7):
        if s[i] == ' ' or s[i] == ',':
            continue
        else:
            m = s[i:i+3].upper() 
            break
        i += 1
    return d, m, y

def date2Days(d, mon):
    days = cumDaysMon[MON[mon]] + d
    return days

def DDMONY42Y4DDD(s):

    d, mon, y = parseDate(s)
    days = date2Days(d, mon)
 
    return y*1000 + days


d1 = "17 Jan 2002"
d2 = "4 March, 2001"
d3 = "4, March,2001"
print(DDMONY42Y4DDD(d1))
print(DDMONY42Y4DDD(d2))
print(DDMONY42Y4DDD(d3))
#print(DDMON2YYYYDDD(d2))
#print(DDMON2YYYYDDD(d3))
